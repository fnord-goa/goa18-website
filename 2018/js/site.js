var openingTimeline = new TimelineMax();
var sundownTimeline = new TimelineMax();
openingTimeline.add([
    TweenMax.to(".grill-layer", 1, {className: '+=grill-top-position'}),
    TweenMax.to(".curtain-left", 1, {xPercent: -100}),
    TweenMax.to(".curtain-right", 1, {xPercent: 100}),
    TweenMax.to(".textroll-layer", 1, {yPercent: -195}),
    TweenMax.to(".landscape-layer", 1, {yPercent: 20}),
]);
sundownTimeline.add([
    TweenMax.to(".background-image-night", 1, {opacity: 1}),
    TweenMax.to(".scroll-arrow", 1, {opacity: 0}),
]);
var controller = new ScrollMagic.Controller();
new ScrollMagic.Scene({triggerElement: ".content-opening", duration: "80%"})
    .setTween(openingTimeline)
    .addTo(controller);
new ScrollMagic.Scene({triggerElement: ".content-flyer", offset: 600, duration: "40%"})
    .setTween(sundownTimeline)
    .addTo(controller);
